# Mandate

AI shallots


# Goal Definition

1. know size of crop/yield

2. size of shallots needs to be in the sweet spot

3. is to find individual shapes

4. handling variation in size of onions

5. handling onions in shadowy areas (alternatively: tweak luminosity and contrast)


## Challenge

1. get a data set that makes sense


## Potential Solutions

1. annotations: run sequestering algorithm. paint remaining images?

2. start with 200 images even and annotate

3. could be quicker by removing background?

4. overlapping onions are the big problem

## Other Ideas

1. transfer learning or original model

2. represent all the variations and scenarios of overlap

3. investigate putting a cover on top to change light. perhaps but not ideal

4. better to have minimal components

5. Res50 as a feature extractor, no pixels in Res 50
